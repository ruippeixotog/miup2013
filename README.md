# Solutions to MIUP 2013 problems

This repository contains solutions to the problems of the [2013 edition][miup2013] of MIUP ("Maratona Inter-Universitária de Programação"). MIUP serves as a preparation of portuguese teams to [SWERC][swerc] (Southwestern Europe Regional Contest). These solutions are provided "as is". I give no guarantees that they will work as expected.

## Instructions

You can compile all the problems by issuing the following command:

    $ make

If you want to compile only a specific problem, issue the following command, replacing `<problem_id>` with the section and id of the problem you want to compile (see section "Problems Solved" for the list of possible ids):

    $ make <problem_id>

Running a compiled problem is just a matter of executing a command similar to the next one, replacing `100` with the id of the desired problem:

    $ ./A

Unless stated otherwise, every problem in this repository reads from the standard input and writes to the standard output.

## Problems Solved

The following is the list of the problems solved. Each problem id is specified between round brackets. Problems marked with ✓ are done, while problems with ✗ are not complete and/or aren't efficient enough.

* ✓ Speed Leaks (`A`)
* ✓ Say Geese (`B`)
* ✓ Roman Warfare (`C`)
* ✓ Base Arithmetic (`D`)
* ✓ Twenty Thirteen (`F`)
* ✓ Polygon Phobia (`G`)
* ✓ No Music, no Life (`H`)

[swerc]: http://swerc.eu
[miup2013]: http://www.fc.ul.pt/pt/conferencia/miup-2013