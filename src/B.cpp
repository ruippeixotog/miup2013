#include <cstring>
#include <iostream>
#include <vector>

#define ll long long
#define bit(b) (1 << (b))

#define MAXT 45
#define MAXN 11

using namespace std;

ll dp[MAXT + 1][bit(MAXN)];

int main() {
  int n, t; cin >> n >> t;
  
  memset(dp, 0, sizeof(dp));
  for(int i = 0; i < n; i++)
    dp[0][bit(i)] = 1;

  for(int i = 1; i <= t; i++) {
    int a, b; cin >> a >> b;

    vector<int> left, right;
    for(int j = 0; j < bit(n); j++) {
      dp[i][j] += dp[i - 1][j];

      if((j & bit(a)) && (j & bit(b))) {
        dp[i][j] += dp[i - 1][j];
      }
      else if(j & bit(a)) {
        left.push_back(j);
        for(int k = 0; k < int(right.size()); k++) {
          if(!(j & right[k]))
            dp[i][j | right[k]] += dp[i - 1][j] * dp[i - 1][right[k]];
        }
      }
      else if(j & bit(b)) {
        right.push_back(j);
        for(int k = 0; k < int(left.size()); k++) {
          if(!(left[k] & j))
            dp[i][left[k] | j] += dp[i - 1][left[k]] * dp[i - 1][j];
        }
      }
    }
  }
  cout << dp[t][bit(n) - 1] << endl;
  return 0;
}
