#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#define MAXA 4000
#define MAXP 4000

using namespace std;

struct State {
  int wealth, dist, maint;
  State(): wealth(0) {} 
  State(int w, int d, int m): wealth(w), dist(d), maint(m) {}

  bool operator<(const State& s) const {
    if(wealth != s.wealth) return wealth < s.wealth;
    if(dist != s.dist) return dist > s.dist;
    return maint > s.maint;
  }
};

State dp[MAXA + 1][MAXP + 1];

pair<int, pair<int, int> > armies[MAXA];
pair<int, pair<int, int> > pops[MAXP];

inline int calcDist(int ai, int pi) {
  return abs(armies[ai].second.first - pops[pi].second.first) +
    abs(armies[ai].second.second - pops[pi].second.second);
}

int main() {
  int a, p; cin >> a >> p;

  for(int i = 0; i < a; i++)
    cin >> armies[i].second.first >>
      armies[i].second.second >> armies[i].first;
  sort(armies, armies + a);

  for(int i = 0; i < p; i++)
    cin >> pops[i].second.first >>
      pops[i].second.second >> pops[i].first;
  sort(pops, pops + p);

  for(int i = 1; i <= a; i++) {
    for(int j = 1; j <= p; j++) {
      State withArmy(
        dp[i - 1][j - 1].wealth + pops[j - 1].first,
        dp[i - 1][j - 1].dist + calcDist(i - 1, j - 1),
        dp[i - 1][j - 1].maint + armies[i - 1].first);

      dp[i][j] = max(dp[i - 1][j], withArmy);
    }
  }
  printf("%d %d %d\n", dp[a][p].wealth, dp[a][p].dist, dp[a][p].maint);
  return 0;
}
