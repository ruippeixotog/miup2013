#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <vector>

#define miter map<string, vector<int> >::iterator

using namespace std;

map<string, vector<int> > cars;

bool sortDec(int a, int b) { return a > b; }

bool sortPlates(string p1, string p2) {
  vector<int>& v1 = cars[p1], & v2 = cars[p2];
  return v1 != v2 ? v1 > v2 : p1 < p2;
}

int main() {
  string plate; int speed;
  while(cin >> plate >> speed)
    cars[plate].push_back(speed);

  vector<string> plates;
  for(miter it = cars.begin(); it != cars.end(); it++) {
    plates.push_back(it->first);
    sort(it->second.begin(), it->second.end(), sortDec);
  }
  sort(plates.begin(), plates.end(), sortPlates);

  for(int i = 0; i < int(plates.size()); i++) {
    cout << plates[i];
    vector<int>& speeds = cars[plates[i]];
    for(int i = 0; i < int(speeds.size()); i++)
      cout << " " << speeds[i];
    cout << endl;
  }
  return 0;
}
