#include <iostream>

using namespace std;

int toInt(string str, int base) {
  int num = 0, digit;
  for(int i = 0; i < int(str.length()); i++) {
    if(str[i] >= '0' && str[i] <= '9') digit = str[i] - '0';
    else digit = str[i] - 'A' + 10;

    if(digit >= base) return -1;
    num = num * base + digit;
  }
  return num;
}

int main() {
  string s1, s2, s3, s4;
  cin >> s1 >> s2 >> s3 >> s4;
  for(int b = 2; b <= 36; b++) {
    int n1 = toInt(s1, b), n2 = toInt(s2, b),
      n3 = toInt(s3, b), n4 = toInt(s4, b);

    if(n1 != -1 && n2 != -1 && n3 != -1 && n4 != -1 &&
      n1 + n2 == n3 * n4) {
      cout << b << endl;
      break;
    }
  }
  return 0;
}
