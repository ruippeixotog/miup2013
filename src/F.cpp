#include <algorithm>
#include <cstring>
#include <iostream>
#include <vector>

#define ll long long

using namespace std;

vector<ll> years[10];
bool used[10];

void gen(ll n, int minDigit, int maxDigit, int sz) {
  if(maxDigit - minDigit + 1 == sz) years[sz - 1].push_back(n);
  if(sz == 10) return;

  for(int i = (sz == 0 ? 1 : 0); i < 10; i++) {
    if(used[i]) continue;
    used[i] = true;
    gen(n * 10 + i, min(minDigit, i), max(maxDigit, i), sz + 1);
    used[i] = false;
  }
}

int main() {
  memset(used, false, sizeof(used));
  gen(0, 10, -1, 0);

  int t; cin >> t;
  while(t--) {
    ll year; cin >> year;

    ll y = year, nd = 0;
    while(y) { nd++; y /= 10; }

    vector<ll>::iterator it =
      upper_bound(years[nd - 1].begin(), years[nd - 1].end(), year);
    ll nextYear = it != years[nd - 1].end() ? *it : years[nd][0];

    cout << nextYear << endl;
  }
  return 0;
}
