#include <iostream>
#include <vector>

using namespace std;

#define REP(i, a, b) for (int i = int(a); i <= int(b); i++)
int pset[1000000];
void initSet() { REP (i, 0, 1000000 - 1) pset[i] = i; }
int findSet(int i) { return (pset[i] == i) ? i : (pset[i] = findSet(pset[i])); }
void unionSet(int i, int j) { pset[findSet(i)] = findSet(j); }
bool isSameSet(int i, int j) { return findSet(i) == findSet(j); }

int main() {
  int s; cin >> s;

  int count = 0;
  initSet();
  while(s--) {
    int x0, y0, x1, y1;
    cin >> x0 >> y0 >> x1 >> y1;
    if(findSet(x0 * 1000 + y0) != findSet(x1 * 1000 + y1)) {
      unionSet(x0 * 1000 + y0, x1 * 1000 + y1);
      count++;
    }
  }
  cout << count << endl;
  return 0;
}
