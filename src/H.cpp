#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#define MAXO 25

using namespace std;

int ni, no;
vector<int> orch[MAXO];

int countOrch(int k, vector<int>& curr, int currSize) {
  if(k == no) return 0;

  int newSize = currSize + orch[k].size();
  bool canAdd = true;
  for(int i = 0; i < int(orch[k].size()); i++)
    if(curr[orch[k][i]]++) canAdd = false;

  int count = 0;
  if(canAdd) {
    count = newSize == ni ? 1 :
      countOrch(k + 1, curr, newSize);
  }

  for(int i = 0; i < int(orch[k].size()); i++)
    curr[orch[k][i]]--;

  return count + countOrch(k + 1, curr, currSize);
}

int main() {
  cin >> ni >> no;

  for(int i = 0; i < no; i++) {
    string str; cin >> str;
    for(int j = 0; j < ni; j++)
      if(str[j] == '1') orch[i].push_back(j);
  }

  vector<int> vec(ni);
  cout << countOrch(0, vec, 0) << endl;
  return 0;
}
